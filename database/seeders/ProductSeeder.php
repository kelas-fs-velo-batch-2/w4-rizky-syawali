<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          
        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 5; $i++){

            DB::table('products')->insert([
                'name' => $faker->name(30),
                'descriptions' => $faker->text(200),
                'created_at' => $faker->dateTime($max = 'now', $timezone = 'Asia/Jakarta'),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = 'Asia/Jakarta'),
                'user_id' => $faker->numberBetween(2,6),
                
            ]);

        }
    }
}
