<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePresencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('employee_id')->nullable();
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->string('timezone')->nullable();
            $table->float('latitude')->nullable();
            $table->float('longitude')->nullable();
            $table->jsonb('metadata')->nullable();
            $table->softDeletes()->nullable();
            $table->index(['employee_id','date'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presences');
    }
}
